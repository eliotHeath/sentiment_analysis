# -*- coding: utf-8 -*-
"""
Codigo fuente del proyecto Horer de los amigos del codigo, usado para el Hackaton BBVA 2020
A mitad de la pandemia, ponemos nuestras esperanzas de ganar estas lineas de codigo.
Per Aspera Ad Adstra...
"""
__title__ = 'Horer'
__author__ = 'Los Amigos del Codigo'


#LLibrearias iniciales
import re
import math
import numpy as np
import pandas as pd

#
import xml.etree.ElementTree as et 
from collections import Counter
import itertools
import collections
from scipy.misc import * 

#Procesamiento Natural
import nltk
from nltk.collocations import BigramCollocationFinder, BigramAssocMeasures
from nltk.corpus import stopwords
from nltk import word_tokenize 
from nltk.stem import WordNetLemmatizer
from nltk.util import ngrams
from nltk import bigrams

#Descargamos algunos objetos que nos ayudaran al analsis de sentimietos
nltk.download('stopwords')
nltk.download('punkt')
stop_words = set(stopwords.words('spanish'))

#Graficas
import matplotlib.pyplot as plt



"""clean_data: 
        |---- data: Acepta una serie Pandas
   Quita caracteres que especiales, asi como signuos de admiracion y de puntuacion.             
   Con esto se hace una limpeza de datos general .
   Resultado: Serie pandas con las caracteriscas deseadas 
"""

def clean_data(data):
        
    data=data.apply(lambda x: str(x).lower())
    data=data.str.replace("año", "anio", regex=False)
    data=data.str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
    data=data.str.replace("!", "", regex=False)
    data=data.str.replace("¡", "", regex=False)
    data=data.str.replace("?", "", regex=False)
    data=data.str.replace("¿", "", regex=False)
    data=data.str.replace(",", "", regex=False)
    data=data.str.replace(".", "", regex=False)
    data=data.str.replace(":)", "", regex=False)
    data=data.str.replace("-", "", regex=False)
    data=data.str.replace("'", "", regex=False)
    data=data.str.replace("\"", " ", regex=False)
    data=data.str.replace(":", "", regex=False)
    data=data.str.replace(":(", "", regex=False)
    data=data.str.replace(r'4', "", regex=False)
    data=data.str.replace(")", " ", regex=False)
    data=data.str.replace("(", " ", regex=False)
    data=data.str.replace(";"," ", regex=False)
    data=data.str.replace(">"," ", regex=False)
    data=data.str.replace("si","", regex=False)
    data=data.str.replace("bbva","", regex=False)
    data=data.str.replace("BBVA","", regex=False)
    data=data.str.replace("Bbva","", regex=False)
    data=data.str.replace("bancomer","", regex=False)
    data=data.str.replace("app","", regex=False)
    data=data.str.replace("mas","", regex=False)    
    data=data.str.replace("@[^s]+", "", regex=True)
    data=data.str.replace("httpS+", "", regex=True)
    data=data.str.replace("https//", "", regex=True)
    data=data.str.replace("buenos", "", regex=False)
    data=data.str.replace("buenas", "", regex=False)
    data=data.str.replace("dias", "", regex=False)
    data=data.str.replace("tardes", "", regex=False)
    data=data.str.replace("hola", "", regex=False)
      
    return data

"""n_gram: 
        |---- data: Acepta una serie Pandas
   Analisis de n-gramamas, con el cual se hace un estudio frecuentista de los datos. 
   Se anade una columna con una metrica bayesiana para darle un mejor enfoque.
   Resutaldo: Una tabla.
"""

def n_gram(data):

    #Guardams los mensajes en una lista
    lista = data.values.tolist()
    
    #Tokenizar 
    tweets_t = [word_tokenize(tweet) for tweet in lista]

    tweets_nsw = [[word for word in tweet_words if not word in stop_words]
                  for tweet_words in tweets_t]
    #Lista sin vacios
    tweets_nsw = list(filter(None, tweets_nsw))
    #Crear bigramas, se varia el dos.
    terms_bigram = [list(ngrams(w_tweet,2)) for w_tweet in tweets_nsw]
    ###
    bigrams = list(itertools.chain(*terms_bigram))
    # Crear el objeto con los bogramas contados
    bigram_counts = collections.Counter(bigrams)
    ##ver 10 primeros bigramas
    bigram_counts.most_common(100)
    ###
    palabras = list(itertools.chain(*tweets_nsw))
    contador=Counter(palabras)

    ###
    c=dict(bigram_counts)
    key=list(c.keys())
    value=list(c.values())
    list1, list2 = zip(*key)
    ##
    c1=dict(contador)
    suma_uni=sum(list(c1.values()))
    ########
    list3=[]
    list4=[]
    ###
    for i in range(0,len(list1)):
       list3.append(c1.get(list1[i]))
    for i in range(0,len(list2)):
       list4.append(c1.get(list2[i]))
    #####

    df1 = pd.DataFrame(list(list1), columns=['palabra1'])
    df1["palabra2"]=pd.DataFrame(list(list2))
    df1["fx"]=pd.DataFrame(list(list3))
    df1["fy"]=pd.DataFrame(list(list4))
    df1["fxy"]=pd.DataFrame(list(value))
    suma_bi=sum(df1["fxy"])
    df1["pfx"]=df1["fx"]/suma_uni
    df1["pfy"]=df1["fy"]/suma_uni
    df1["pfxy"]=df1["fxy"]/suma_bi
    df1["Ixy"]=df1["pfxy"]/(df1["pfx"]*df1["pfy"])
    df1=df1[df1["fxy"]>=5]
    df1["Ixy"]=np.log(df1["Ixy"])
    df1=df1.sort_values('fxy', ascending=False)
    df1.reset_index(drop=True, inplace=True)
    
    return df1

"""Word_Cloud: 
        |---- texto: Acepta ----
   Funcionn que genera una nube de pabra, la cual se integra al dashboard     
   Resutaldo: Nube de palabras.
"""

def Word_Cloud(texto):

    #lista = list(texto)
    #lista = texto.values.tolist()
    lista = texto["texto"].values.tolist()
    ###
    tweets_t = [word_tokenize(tweet) for tweet in lista]
    ########
    tweets_nsw = [[word for word in tweet_words if not word in stop_words]
                  for tweet_words in tweets_t]
    #####
    #lista sin vacíos
    #tweets_nsw = list(filter(None, tweets_nsw))
    #Crear bigramas, se varia el dos.
    tweets_nsw=[" ".join(x) for x in tweets_nsw]
    texto2 = pd.DataFrame(tweets_nsw,columns=['texto2'])
    texto = pd.concat([texto, texto2], axis=1)


    neg_tweets = texto[texto.sentimiento == 0]
    neg_string = []
    for t in neg_tweets.texto2:
        neg_string.append(t)
    neg_string = pd.Series(neg_string).str.cat(sep=' ')
    from wordcloud import WordCloud

    wordcloud = WordCloud(width=1600, height=800,max_font_size=200).generate(neg_string)
    plt.figure(figsize=(12,10))
    plt.imshow(wordcloud, interpolation="bilinear")
    plt.axis("off")
    plt.show()

    pos_tweets = texto[texto.sentimiento == 1]
    pos_string = []
    for t in pos_tweets.texto2:
        pos_string.append(t)
    pos_string = pd.Series(pos_string).str.cat(sep=' ')

    wordcloud = WordCloud(width=1600, height=800,max_font_size=200).generate(pos_string)
    plt.figure(figsize=(12,10))
    plt.imshow(wordcloud, interpolation="bilinear")
    plt.axis("off")
    plt.show()
    texto=texto[(texto["sentimiento"]==1) | (texto["sentimiento"]==0)]
    texto=texto.sort_values(by=['sentimiento'], ascending=True)
    texto.reset_index(drop=True, inplace=True)
    
    return texto