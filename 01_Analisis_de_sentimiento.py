#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 16 01:34:37 2020

@author: francisco
"""
import xml.etree.ElementTree as et 
from lxml import objectify
from pandas import DataFrame
import matplotlib
######
import nltk
from nltk.collocations import BigramCollocationFinder, BigramAssocMeasures
from collections import Counter
from nltk.corpus import stopwords
####
from nltk import word_tokenize 
from nltk.stem import WordNetLemmatizer
from nltk.util import ngrams
from nltk import bigrams
import math
import numpy as np
import pandas as pd
import itertools
import collections
nltk.download('stopwords')
stop_words = set(stopwords.words('spanish'))
import matplotlib.pyplot as plt
import os
###########
########
directorio=os.getcwd()
diccionario=directorio+"/insumos/TASS2019_country_MX_train.xml"
print(diccionario)
xtree = et.parse(diccionario)
xroot = xtree.getroot()
######
file_path= directorio +"/insumos/utf8.csv"
data1=df = pd.read_csv(file_path)
#####
texto=pd.DataFrame(columns=["texto","sentimiento"])
for node in xroot: 
    #print(node)
    s_tweet = node.find("content").text
    s_sentiment = node[5][0][0].text
    df={'texto': [s_tweet],
        'sentimiento': [s_sentiment]}
    df=pd.DataFrame(df, columns=["texto","sentimiento"])
    texto=pd.concat([texto,df])
texto.reset_index(drop=True, inplace=True)
texto1=texto
#####
texto= data1.append(texto)
texto.reset_index(drop=True, inplace=True)

######
texto=texto.apply(lambda x: x.astype(str).str.lower())
texto["texto"]=texto["texto"].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
texto["texto"]=texto["texto"].str.replace("!", "", regex=False)
texto["texto"]=texto["texto"].str.replace("¡", "", regex=False)
texto["texto"]=texto["texto"].str.replace(":)", "", regex=False)
texto["texto"]=texto["texto"].str.replace(":(", "", regex=False)
texto["texto"]=texto["texto"].str.replace("?", "", regex=False)
texto["texto"]=texto["texto"].str.replace("¿", "", regex=False)
texto["texto"]=texto["texto"].str.replace("-", "", regex=False)
texto["texto"]=texto["texto"].str.replace(".", " ", regex=False)
texto["texto"]=texto["texto"].str.replace(",", " ", regex=False)
texto["texto"]=texto["texto"].str.replace("'", "", regex=False)
texto["texto"]=texto["texto"].str.replace("\"", " ", regex=False)
texto["texto"]=texto["texto"].str.replace(")", " ", regex=False)
texto["texto"]=texto["texto"].str.replace("(", " ", regex=False)
texto["texto"]=texto["texto"].str.replace(";"," ", regex=False)
texto["texto"]=texto["texto"].str.replace(":"," ", regex=False)
texto["texto"]=texto["texto"].str.replace(">"," ", regex=False)
#########
#texto["texto"]=texto["texto"].str.replace("si"," ", regex=False)
texto["texto"]=texto["texto"].str.replace("bbva"," ", regex=False)
texto["texto"]=texto["texto"].str.replace("BBVA"," ", regex=False)
texto["texto"]=texto["texto"].str.replace("banco"," ", regex=False)
texto["texto"]=texto["texto"].str.replace("servicio"," ", regex=False)

texto["texto"]=texto["texto"].str.replace("@[^s]+", "", regex=True)
texto["texto"]=texto["texto"].str.replace("httpS+", "", regex=True)
texto=texto.astype(str).apply(lambda x: x.str.encode('ascii', 'ignore').str.decode('ascii'))
#texto['texto'] = texto['texto'].apply(lambda x: ' '.join([word for word in x.split() if word not in (stop_words)]))
#texto['texto'] = texto['texto'].apply(lambda x: ' '.join([word for word in x.split() if word not in (["si"])]))
texto['texto'].replace('', np.nan, inplace=True)
texto=texto.dropna(subset=["texto"])
texto.sentimiento[texto.sentimiento == 'none'] = "neu"
texto.reset_index(drop=True, inplace=True)


lista = texto["texto"].values.tolist()
###
tweets_t = [word_tokenize(tweet) for tweet in lista]
########
tweets_nsw = [[word for word in tweet_words if not word in stop_words]
              for tweet_words in tweets_t]
#####
#lista sin vacíos
#tweets_nsw = list(filter(None, tweets_nsw))
#Crear bigramas, se varia el dos.
tweets_nsw=[" ".join(x) for x in tweets_nsw]
texto2 = DataFrame(tweets_nsw,columns=['texto2'])
texto = pd.concat([texto, texto2], axis=1)


neg_tweets = texto[texto.sentimiento == "n"]
neg_string = []
for t in neg_tweets.texto2:
    neg_string.append(t)
neg_string = pd.Series(neg_string).str.cat(sep=' ')
from wordcloud import WordCloud

wordcloud = WordCloud(width=1600, height=800,max_font_size=200).generate(neg_string)
plt.figure(figsize=(12,10))
plt.imshow(wordcloud, interpolation="bilinear")
plt.axis("off")
plt.show()

pos_tweets = texto[texto.sentimiento == "p"]
pos_string = []
for t in pos_tweets.texto2:
    pos_string.append(t)
pos_string = pd.Series(pos_string).str.cat(sep=' ')

wordcloud = WordCloud(width=1600, height=800,max_font_size=200).generate(pos_string)
plt.figure(figsize=(12,10))
plt.imshow(wordcloud, interpolation="bilinear")
plt.axis("off")
plt.show()
texto=texto[(texto["sentimiento"]=='p') | (texto["sentimiento"]=='n')]
texto=texto.sort_values(by=['sentimiento'], ascending=True)
texto.reset_index(drop=True, inplace=True)

#########
import sklearn
from sklearn.feature_extraction.text import CountVectorizer
cvec = CountVectorizer()
cvec.fit(texto.texto2)
len(cvec.get_feature_names())

#########
neg_doc_matrix = cvec.transform(texto[texto.sentimiento == "n"].texto2)
pos_doc_matrix = cvec.transform(texto[texto.sentimiento == "p"].texto2)
#neu_doc_matrix = cvec.transform(texto[texto.sentimiento == "neu"].texto2)
neg_tf = np.sum(neg_doc_matrix,axis=0)
pos_tf = np.sum(pos_doc_matrix,axis=0)
neg = np.squeeze(np.asarray(neg_tf))
pos = np.squeeze(np.asarray(pos_tf))
term_freq_df = pd.DataFrame([neg,pos],columns=cvec.get_feature_names()).transpose()
########
document_matrix = cvec.transform(texto.texto2)
texto[texto.sentimiento == "n"].tail()
###

########
neg_batches = np.linspace(0,698,100).astype(int)
i=0
neg_tf = []
while i < len(neg_batches)-1:
    batch_result = np.sum(document_matrix[neg_batches[i]:neg_batches[i+1]].toarray(),axis=0)
    neg_tf.append(batch_result)
    if (i % 10 == 0) | (i == len(neg_batches)-2):
        print(neg_batches[i+1],"entries' term freuquency calculated")
    i += 1
##
pos_batches = np.linspace(600,920,100).astype(int)
i=0
pos_tf = []
while i < len(pos_batches)-1:
    batch_result = np.sum(document_matrix[pos_batches[i]:pos_batches[i+1]].toarray(),axis=0)
    pos_tf.append(batch_result)
    if (i % 10 == 0) | (i == len(pos_batches)-2):
        print(pos_batches[i+1],"entries' term freuquency calculated")
    i += 1
#######
neg = np.sum(neg_tf,axis=0)
pos = np.sum(pos_tf,axis=0)
term_freq_df2 = pd.DataFrame([neg,pos],columns=cvec.get_feature_names()).transpose()
term_freq_df2.columns = ['negative', 'positive']
term_freq_df2['total'] = term_freq_df2['negative'] + term_freq_df2['positive']
term_freq_df2.sort_values(by='total', ascending=False).iloc[:10]
######
y_pos = np.arange(50)
plt.figure(figsize=(12,10))
plt.bar(y_pos, term_freq_df2.sort_values(by='negative', ascending=False)['negative'][:50], align='center', alpha=0.5)
plt.xticks(y_pos, term_freq_df2.sort_values(by='negative', ascending=False)['negative'][:50].index,rotation='vertical')
plt.ylabel('Frequency')
plt.xlabel('Top 50 palabras negativas')
plt.title('Top 50 palabras negativas')
####
y_pos = np.arange(50)
plt.figure(figsize=(12,10))
plt.bar(y_pos, term_freq_df2.sort_values(by='positive', ascending=False)['positive'][:50], align='center', alpha=0.5)
plt.xticks(y_pos, term_freq_df2.sort_values(by='positive', ascending=False)['positive'][:50].index,rotation='vertical')
plt.ylabel('Frequency')
plt.xlabel('Top 50 palabras positivas')
plt.title('Top 50 palabras positivas')
#####
import seaborn as sns
plt.figure(figsize=(8,6))
ax = sns.regplot(x="negative", y="positive",fit_reg=False, scatter_kws={'alpha':0.5},data=term_freq_df2)
plt.ylabel('Positive Frequency')
plt.xlabel('Negative Frequency')
plt.title('Negative Frequency vs Positive Frequency')
#########
term_freq_df2['pos_rate'] = term_freq_df2['positive'] * 1./term_freq_df2['total']
term_freq_df2.sort_values(by='pos_rate', ascending=False).iloc[:10]
########
term_freq_df2['pos_freq_pct'] = term_freq_df2['positive'] * 1./term_freq_df2['positive'].sum()
term_freq_df2.sort_values(by='pos_freq_pct', ascending=False).iloc[:10]
####
from scipy.stats import hmean

term_freq_df2['pos_hmean'] = term_freq_df2.apply(lambda x: (hmean([x['pos_rate'], x['pos_freq_pct']])
                                                                   if x['pos_rate'] > 0 and x['pos_freq_pct'] > 0 
                                                                   else 0), axis=1)                                                        
term_freq_df2.sort_values(by='pos_hmean', ascending=False).iloc[:10]
############
from scipy.stats import norm
def normcdf(x):
    return norm.cdf(x, x.mean(), x.std())

term_freq_df2['pos_rate_normcdf'] = normcdf(term_freq_df2['pos_rate'])
term_freq_df2['pos_freq_pct_normcdf'] = normcdf(term_freq_df2['pos_freq_pct'])
####
term_freq_df2['pos_rate_normcdf'].fillna(.001, inplace = True)
term_freq_df2['pos_freq_pct_normcdf'].fillna(.001, inplace = True)
#####
term_freq_df2['pos_normcdf_hmean'] = hmean([term_freq_df2['pos_rate_normcdf'], term_freq_df2['pos_freq_pct_normcdf']])
term_freq_df2.sort_values(by='pos_normcdf_hmean', ascending=False).iloc[:10]
#####################
term_freq_df2['neg_rate'] = term_freq_df2['negative'] * 1./term_freq_df2['total']
term_freq_df2['neg_freq_pct'] = term_freq_df2['negative'] * 1./term_freq_df2['negative'].sum()

term_freq_df2['neg_hmean'] = term_freq_df2.apply(lambda x: (hmean([x['neg_rate'], x['neg_freq_pct']])
                                                                   if x['neg_rate'] > 0 and x['neg_freq_pct'] > 0 
                                                                   else 0), axis=1)                                                        
term_freq_df2['neg_rate_normcdf'] = normcdf(term_freq_df2['neg_rate'])
term_freq_df2['neg_freq_pct_normcdf'] = normcdf(term_freq_df2['neg_freq_pct'])
term_freq_df2['neg_rate_normcdf'].fillna(.001, inplace = True)
term_freq_df2['neg_freq_pct_normcdf'].fillna(.001, inplace = True)
term_freq_df2['neg_normcdf_hmean'] = hmean([term_freq_df2['neg_rate_normcdf'], term_freq_df2['neg_freq_pct_normcdf']])
term_freq_df2.sort_values(by='neg_normcdf_hmean', ascending=False).iloc[:10]
#####
plt.figure(figsize=(8,6))
ax = sns.regplot(x="neg_hmean", y="pos_hmean",fit_reg=False, scatter_kws={'alpha':0.5},data=term_freq_df2)
plt.ylabel('Positive Rate and Frequency Harmonic Mean')
plt.xlabel('Negative Rate and Frequency Harmonic Mean')
plt.title('neg_hmean vs pos_hmean')
#####
plt.figure(figsize=(8,6))
ax = sns.regplot(x="neg_normcdf_hmean", y="pos_normcdf_hmean",fit_reg=False, scatter_kws={'alpha':0.5},data=term_freq_df2)
plt.ylabel('Positive Rate and Frequency CDF Harmonic Mean')
plt.xlabel('Negative Rate and Frequency CDF Harmonic Mean')
plt.title('neg_normcdf_hmean vs pos_normcdf_hmean')
########
#######
from bokeh.plotting import figure
from bokeh.io import output_notebook, show
from bokeh.models import LinearColorMapper
output_notebook()
color_mapper = LinearColorMapper(palette='Inferno256', low=min(term_freq_df2.pos_normcdf_hmean), high=max(term_freq_df2.pos_normcdf_hmean))
p = figure(x_axis_label='neg_normcdf_hmean', y_axis_label='pos_normcdf_hmean')
p.circle('neg_normcdf_hmean','pos_normcdf_hmean',size=5,alpha=0.3,source=term_freq_df2,color={'field': 'pos_normcdf_hmean', 'transform': color_mapper})
from bokeh.models import HoverTool
hover = HoverTool(tooltips=[('token','@index')])
p.add_tools(hover)
show(p)
#######
######
texto['sentimiento2']=texto['sentimiento']
texto.sentimiento2[texto.sentimiento == 'p'] = 1 
texto.sentimiento2[texto.sentimiento == 'n'] = 0 
x = texto.texto2
y = texto.sentimiento2
from sklearn.model_selection import train_test_split
SEED = 2000
x_train, x_validation_and_test, y_train, y_validation_and_test = train_test_split(x, y, test_size=.2, random_state=SEED)
x_validation, x_test, y_validation, y_test = train_test_split(x_validation_and_test, y_validation_and_test, test_size=.5, random_state=SEED)
print("Train set has total {0} entries with {1:.2f}% negative, {2:.2f}% positive".format(len(x_train),(len(x_train[y_train == 1]) / (len(x_train)*1.))*100,(len(x_train[y_train == 0]) / (len(x_train)*1.))*100))
print("Validation set has total {0} entries with {1:.2f}% negative, {2:.2f}% positive".format(len(x_validation), (len(x_validation[y_validation == 1]) / (len(x_validation)*1.))*100, (len(x_validation[y_validation == 0]) / (len(x_validation)*1.))*100))
print ("Test set has total {0} entries with {1:.2f}% negative, {2:.2f}% positive".format(len(x_test), (len(x_test[y_test == 1]) / (len(x_test)*1.))*100,(len(x_test[y_test == 0]) / (len(x_test)*1.))*100))
##############
from textblob import TextBlob
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report, confusion_matrix
tbresult = [TextBlob(i).sentiment.polarity for i in x_validation]
tbpred = [0 if n < 0 else 1 for n in tbresult]
y1_validation=y_validation.copy()
y1_validation.reset_index(drop=True, inplace=True)
y1_validation=y1_validation.values.tolist()
conmat = np.array(confusion_matrix(y1_validation, tbpred, labels=[1,0]))
confusion = pd.DataFrame(conmat, index=['positive', 'negative'],
                         columns=['predicted_positive','predicted_negative'])
print("Accuracy Score: {0:.2f}%".format(accuracy_score(y1_validation, tbpred)*100))
print("-"*80)
print("Confusion Matrix\n")
print(confusion)
print("-"*80)
print("Classification Report\n")
print(classification_report(y1_validation, tbpred))
#######################

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
from time import time

def accuracy_summary(pipeline, x_train, y_train, x_test, y_test):
    print("cálculo de accuracy")
    y_train=y_train.astype('int')
    if len(x_test[y_test == 0]) / (len(x_test)*1.) > 0.5:
        null_accuracy = len(x_test[y_test == 0]) / (len(x_test)*1.)
    else:
        null_accuracy = 1. - (len(x_test[y_test == 0]) / (len(x_test)*1.))
    t0 = time()
    sentiment_fit = pipeline.fit(x_train, y_train)
    y_pred = sentiment_fit.predict(x_test)
    train_test_time = time() - t0
    y_test=y_test.astype('int')
    y_pred=y_pred.astype('int')
    accuracy = accuracy_score(y_test, y_pred)
    print("null accuracy: {0:.2f}%".format(null_accuracy*100))
    print("accuracy score: {0:.2f}%".format(accuracy*100))
    if accuracy > null_accuracy:
        print("model is {0:.2f}% more accurate than null accuracy".format((accuracy-null_accuracy)*100))
    elif accuracy == null_accuracy:
        print("model has the same accuracy with the null accuracy")
    else:
        print("model is {0:.2f}% less accurate than null accuracy".format((null_accuracy-accuracy)*100))
    print("train and test time: {0:.2f}s".format(train_test_time))
    print("-"*80)
    return accuracy, train_test_time

cvec = CountVectorizer()
lr = LogisticRegression()
n_features = np.arange(10,1000,10)

def nfeature_accuracy_checker(vectorizer=cvec, n_features=n_features, stop_words=None, ngram_range=(1, 1), classifier=lr):
    result = []
    print(classifier)
    print("\n")
    for n in n_features:
        vectorizer.set_params(stop_words=stop_words, max_features=n, ngram_range=ngram_range)
        checker_pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
        print("Validation result for {} features".format(n))
        nfeature_accuracy,tt_time = accuracy_summary(checker_pipeline, x_train, y_train, x_validation, y_validation)
        result.append((n,nfeature_accuracy,tt_time))
    return result

print("RESULT FOR UNIGRAM without STOP WORDS\n")
feature_result_ug = nfeature_accuracy_checker()
print("RESULT FOR BIGRAM without STOP WORDS\n")
feature_result_bg = nfeature_accuracy_checker(ngram_range=(1, 2))
print("RESULT FOR TRIGRAM without STOP WORDS\n")
feature_result_tg = nfeature_accuracy_checker(ngram_range=(1, 3))

####
nfeatures_plot_tg = pd.DataFrame(feature_result_tg,columns=['nfeatures','validation_accuracy','train_test_time'])
nfeatures_plot_bg = pd.DataFrame(feature_result_bg,columns=['nfeatures','validation_accuracy','train_test_time'])
nfeatures_plot_ug = pd.DataFrame(feature_result_ug,columns=['nfeatures','validation_accuracy','train_test_time'])

plt.figure(figsize=(5,6))
plt.plot(nfeatures_plot_tg.nfeatures, nfeatures_plot_tg.validation_accuracy,label='trigram')
plt.plot(nfeatures_plot_bg.nfeatures, nfeatures_plot_bg.validation_accuracy,label='bigram')
plt.plot(nfeatures_plot_ug.nfeatures, nfeatures_plot_ug.validation_accuracy, label='unigram')
plt.title("N-gram(1~3) test result : Accuracy")
plt.xlabel("Number of features")
plt.ylabel("Validation set accuracy")
plt.legend()

########
def train_test_and_evaluate(pipeline, x_train, y_train, x_test, y_test):
    print("cálculo de matriz")
    y_train=y_train.astype('int')
    if len(x_test[y_test == 0]) / (len(x_test)*1.) > 0.5:
        null_accuracy = len(x_test[y_test == 0]) / (len(x_test)*1.)
    else:
        null_accuracy = 1. - (len(x_test[y_test == 0]) / (len(x_test)*1.))
    sentiment_fit = pipeline.fit(x_train, y_train)
    y_pred = sentiment_fit.predict(x_test)
    y_test=y_test.astype('int')
    y_pred=y_pred.astype('int')
    accuracy = accuracy_score(y_test, y_pred)
    ####
    y_test1=y_test.copy()
    y_test1.reset_index(drop=True, inplace=True)
    y_test1=y_test1.values.tolist()    
    ####
    conmat = np.array(confusion_matrix(y_test1, y_pred, labels=[0,1]))
    confusion = pd.DataFrame(conmat, index=['negative', 'positive'],
                         columns=['predicted_negative','predicted_positive'])
    print("null accuracy: {0:.2f}%".format(null_accuracy*100))
    print("accuracy score: {0:.2f}%".format(accuracy*100))
    if accuracy > null_accuracy:
        print("model is {0:.2f}% more accurate than null accuracy".format((accuracy-null_accuracy)*100))
    elif accuracy == null_accuracy:
        print("model has the same accuracy with the null accuracy")
    else:
        print("model is {0:.2f}% less accurate than null accuracy".format((null_accuracy-accuracy)*100))
    print("-"*80)
    print("Confusion Matrix\n")
    print(confusion)
    print("-"*80)
    print("Classification Report\n")
    print(classification_report(y_test, y_pred, target_names=['negative','positive']))
####
ug_cvec = CountVectorizer(max_features=1000)
ug_pipeline = Pipeline([
        ('vectorizer', ug_cvec),
        ('classifier', lr)
    ])
train_test_and_evaluate(ug_pipeline, x_train, y_train, x_validation, y_validation)
####
bg_cvec = CountVectorizer(max_features=1000,ngram_range=(1, 2))
bg_pipeline = Pipeline([
        ('vectorizer', bg_cvec),
        ('classifier', lr)
    ])
train_test_and_evaluate(bg_pipeline, x_train, y_train, x_validation, y_validation)
####
tg_cvec = CountVectorizer(max_features=10000,ngram_range=(1, 3))
tg_pipeline = Pipeline([
        ('vectorizer', tg_cvec),
        ('classifier', lr)
    ])
train_test_and_evaluate(tg_pipeline, x_train, y_train, x_validation, y_validation)

######
from sklearn.feature_extraction.text import TfidfVectorizer
tvec = TfidfVectorizer()

print("RESULT FOR UNIGRAM WITH STOP WORDS (Tfidf)\n")
feature_result_ugt = nfeature_accuracy_checker(vectorizer=tvec)
print("RESULT FOR BIGRAM WITH STOP WORDS (Tfidf)\n")
feature_result_bgt = nfeature_accuracy_checker(vectorizer=tvec,ngram_range=(1, 2))
print("RESULT FOR TRIGRAM WITH STOP WORDS (Tfidf)\n")
feature_result_tgt = nfeature_accuracy_checker(vectorizer=tvec,ngram_range=(1, 3))

#####
nfeatures_plot_tgt = pd.DataFrame(feature_result_tgt,columns=['nfeatures','validation_accuracy','train_test_time'])
nfeatures_plot_bgt = pd.DataFrame(feature_result_bgt,columns=['nfeatures','validation_accuracy','train_test_time'])
nfeatures_plot_ugt = pd.DataFrame(feature_result_ugt,columns=['nfeatures','validation_accuracy','train_test_time'])

plt.figure(figsize=(8,6))
plt.plot(nfeatures_plot_tgt.nfeatures, nfeatures_plot_tgt.validation_accuracy,label='trigrama (vectorización entropia)',color='royalblue')
plt.plot(nfeatures_plot_tg.nfeatures, nfeatures_plot_tg.validation_accuracy,label='trigrama (vectorización frecuentista)',linestyle=':', color='royalblue')
plt.plot(nfeatures_plot_bgt.nfeatures, nfeatures_plot_bgt.validation_accuracy,label='bigrama (vectorización entropia)',color='orangered')
plt.plot(nfeatures_plot_bg.nfeatures, nfeatures_plot_bg.validation_accuracy,label='bigrama (vectorización frecuentista)',linestyle=':',color='orangered')
plt.plot(nfeatures_plot_ugt.nfeatures, nfeatures_plot_ugt.validation_accuracy, label='unigrama (vectorización entropia)',color='gold')
plt.plot(nfeatures_plot_ug.nfeatures, nfeatures_plot_ug.validation_accuracy, label='unigrama (vectorización frecuentista)',linestyle=':',color='gold')
plt.title("enegrama(1~3) resultados: Accuracy")
plt.xlabel("Number of features")
plt.ylabel("Validation set accuracy")
plt.legend()

####
from sklearn.svm import LinearSVC
from sklearn.ensemble import AdaBoostClassifier
from sklearn.naive_bayes import MultinomialNB, BernoulliNB
from sklearn.linear_model import RidgeClassifier
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.linear_model import Perceptron
from sklearn.neighbors import NearestCentroid
from sklearn.feature_selection import SelectFromModel

names = ["Logistic Regression", "Linear SVC", "LinearSVC with L1-based feature selection","Multinomial NB", 
         "Bernoulli NB", "Ridge Classifier", "AdaBoost", "Perceptron","Passive-Aggresive", "Nearest Centroid"]
classifiers = [
    LogisticRegression(),
    LinearSVC(),
    Pipeline([
  ('feature_selection', SelectFromModel(LinearSVC(penalty="l1", dual=False))),
  ('classification', LinearSVC(penalty="l2"))]),
    MultinomialNB(),
    BernoulliNB(),
    RidgeClassifier(),
    AdaBoostClassifier(),
    Perceptron(),
    PassiveAggressiveClassifier(),
    NearestCentroid()
    ]
zipped_clf = zip(names,classifiers)

tvec = TfidfVectorizer()
def classifier_comparator(vectorizer=cvec, n_features=1000, stop_words=None, ngram_range=(1, 1), classifier=zipped_clf):
    result = []
    vectorizer.set_params(stop_words=stop_words, max_features=n_features, ngram_range=ngram_range)
    for n,c in classifier:
        checker_pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', c)
        ])
        print("Validation result for {}".format(n))
        print(c)
        clf_accuracy,tt_time = accuracy_summary(checker_pipeline, x_train, y_train, x_validation, y_validation)
        result.append((n,clf_accuracy,tt_time))
    return result

a1_unigram_result =classifier_comparator(n_features=1000,ngram_range=(1,1))
b01_unigrama = DataFrame (a1_unigram_result,columns=['model', 'accuracy', 'train and test time'])

names = ["Logistic Regression", "Linear SVC", "LinearSVC with L1-based feature selection","Multinomial NB", 
         "Bernoulli NB", "Ridge Classifier", "AdaBoost", "Perceptron","Passive-Aggresive", "Nearest Centroid"]
classifiers = [
    LogisticRegression(),
    LinearSVC(),
    Pipeline([
  ('feature_selection', SelectFromModel(LinearSVC(penalty="l1", dual=False))),
  ('classification', LinearSVC(penalty="l2"))]),
    MultinomialNB(),
    BernoulliNB(),
    RidgeClassifier(),
    AdaBoostClassifier(),
    Perceptron(),
    PassiveAggressiveClassifier(),
    NearestCentroid()
    ]
zipped_clf = zip(names,classifiers)

tvec = TfidfVectorizer()
def classifier_comparator(vectorizer=cvec, n_features=1000, stop_words=None, ngram_range=(1, 1), classifier=zipped_clf):
    result = []
    vectorizer.set_params(stop_words=stop_words, max_features=n_features, ngram_range=ngram_range)
    for n,c in classifier:
        checker_pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', c)
        ])
        print("Validation result for {}".format(n))
        print(c)
        clf_accuracy,tt_time = accuracy_summary(checker_pipeline, x_train, y_train, x_validation, y_validation)
        result.append((n,clf_accuracy,tt_time))
    return result

a2_bigram_result =classifier_comparator(n_features=1000,ngram_range=(1,2))
b02_bigrama = DataFrame (a2_bigram_result,columns=['model', 'accuracy', 'train and test time'])


names = ["Logistic Regression", "Linear SVC", "LinearSVC with L1-based feature selection","Multinomial NB", 
         "Bernoulli NB", "Ridge Classifier", "AdaBoost", "Perceptron","Passive-Aggresive", "Nearest Centroid"]
classifiers = [
    LogisticRegression(),
    LinearSVC(),
    Pipeline([
  ('feature_selection', SelectFromModel(LinearSVC(penalty="l1", dual=False))),
  ('classification', LinearSVC(penalty="l2"))]),
    MultinomialNB(),
    BernoulliNB(),
    RidgeClassifier(),
    AdaBoostClassifier(),
    Perceptron(),
    PassiveAggressiveClassifier(),
    NearestCentroid()
    ]
zipped_clf = zip(names,classifiers)

tvec = TfidfVectorizer()
def classifier_comparator(vectorizer=cvec, n_features=1000, stop_words=None, ngram_range=(1, 1), classifier=zipped_clf):
    result = []
    vectorizer.set_params(stop_words=stop_words, max_features=n_features, ngram_range=ngram_range)
    for n,c in classifier:
        checker_pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', c)
        ])
        print("Validation result for {}".format(n))
        print(c)
        clf_accuracy,tt_time = accuracy_summary(checker_pipeline, x_train, y_train, x_validation, y_validation)
        result.append((n,clf_accuracy,tt_time))
    return result



a3_trigram_result = classifier_comparator(n_features=1000,ngram_range=(1,3))
b03_trigrama = DataFrame (a3_trigram_result,columns=['model', 'accuracy', 'train and test time'])

#buscar=texto[texto['texto2'].str.contains("mo")]
directorio=os.getcwd()
file_path=directorio+"/insumos/bbva01.csv"
file_path2=directorio+"/insumos/bbva02.csv"
print("Cargar comentarios")
datos_BBVA=df = pd.read_csv(file_path, sep=",")
datos_BBVA2=df = pd.read_csv(file_path2, sep=";")
#####
########
datos_BBVA=datos_BBVA.append(datos_BBVA2)
datos_BBVA=datos_BBVA.dropna(subset=["message"])
datos_BBVA.reset_index(drop=True, inplace=True)
datos_BBVA["message2"]=datos_BBVA["message"]
#######
datos_BBVA=datos_BBVA.apply(lambda x: x.astype(str).str.lower())
datos_BBVA["message"]=datos_BBVA["message"].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
datos_BBVA["message"]=datos_BBVA["message"].str.replace("!", "", regex=False)
datos_BBVA["message"]=datos_BBVA["message"].str.replace("¡", "", regex=False)
datos_BBVA["message"]=datos_BBVA["message"].str.replace(":)", "", regex=False)
datos_BBVA["message"]=datos_BBVA["message"].str.replace(":(", "", regex=False)
datos_BBVA["message"]=datos_BBVA["message"].str.replace("?", "", regex=False)
datos_BBVA["message"]=datos_BBVA["message"].str.replace("¿", "", regex=False)
datos_BBVA["message"]=datos_BBVA["message"].str.replace("-", "", regex=False)
datos_BBVA["message"]=datos_BBVA["message"].str.replace(".", " ", regex=False)
datos_BBVA["message"]=datos_BBVA["message"].str.replace(",", " ", regex=False)
datos_BBVA["message"]=datos_BBVA["message"].str.replace("'", "", regex=False)
datos_BBVA["message"]=datos_BBVA["message"].str.replace("\"", " ", regex=False)
datos_BBVA["message"]=datos_BBVA["message"].str.replace(")", " ", regex=False)
datos_BBVA["message"]=datos_BBVA["message"].str.replace("(", " ", regex=False)
datos_BBVA["message"]=datos_BBVA["message"].str.replace(";"," ", regex=False)
datos_BBVA["message"]=datos_BBVA["message"].str.replace(":"," ", regex=False)
datos_BBVA["message"]=datos_BBVA["message"].str.replace(">"," ", regex=False)
#########
#datos_BBVA["message"]=datos_BBVA["message"].str.replace("si"," ", regex=False)
datos_BBVA["message"]=datos_BBVA["message"].str.replace("bbva"," ", regex=False)
datos_BBVA["message"]=datos_BBVA["message"].str.replace("BBVA"," ", regex=False)
datos_BBVA["message"]=datos_BBVA["message"].str.replace("banco"," ", regex=False)
datos_BBVA["message"]=datos_BBVA["message"].str.replace("servicio"," ", regex=False)

datos_BBVA["message"]=datos_BBVA["message"].str.replace("@[^s]+", "", regex=True)
datos_BBVA["message"]=datos_BBVA["message"].str.replace("httpS+", "", regex=True)
datos_BBVA=datos_BBVA.astype(str).apply(lambda x: x.str.encode('ascii', 'ignore').str.decode('ascii'))
#datos_BBVA['datos_BBVA'] = datos_BBVA['datos_BBVA'].apply(lambda x: ' '.join([word for word in x.split() if word not in (stop_words)]))
#datos_BBVA['datos_BBVA'] = datos_BBVA['datos_BBVA'].apply(lambda x: ' '.join([word for word in x.split() if word not in (["si"])]))
datos_BBVA["message"].replace('', np.nan, inplace=True)
datos_BBVA=datos_BBVA.dropna(subset=["message"])
datos_BBVA.reset_index(drop=True, inplace=True)
tamanio=[]
for element in datos_BBVA["message"]:
    tamanio.append(len(element.split()))
tamanio = pd.DataFrame(data=tamanio, columns=["tamanio"])
datos_BBVA = pd.concat([datos_BBVA, tamanio], axis=1)
datos_BBVA=datos_BBVA[datos_BBVA["tamanio"]>1]
datos_BBVA.reset_index(drop=True, inplace=True)
####
datos_BBVA=datos_BBVA.drop_duplicates(subset=['message'])
####
datos_BBVA.reset_index(drop=True, inplace=True)
#####
lista = datos_BBVA["message"].values.tolist()
###
tweets_t = [word_tokenize(tweet) for tweet in lista]
########
tweets_nsw = [[word for word in tweet_words if not word in stop_words]
              for tweet_words in tweets_t]
#####
#lista sin vacíos
#tweets_nsw = list(filter(None, tweets_nsw))
#Crear bigramas, se varia el dos.
tweets_nsw=[" ".join(x) for x in tweets_nsw]
datos_BBVA2 = DataFrame(tweets_nsw,columns=['datos_BBVA2'])
datos_BBVA = pd.concat([datos_BBVA, datos_BBVA2], axis=1)
###########
##########
type(x_test)
x_test_BBVA=datos_BBVA['datos_BBVA2']
cvec = CountVectorizer()
lr = LogisticRegression()

vectorizer=cvec
n_features=1000
classifier=lr
ngram_range=(1, 1)
vectorizer.set_params(max_features=n_features, ngram_range=ngram_range)

pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
y_train=y_train.astype('int')       
sentiment_fit = pipeline.fit(x_train, y_train)
y_pred = sentiment_fit.predict(x_test_BBVA)
y_pred=y_pred.astype('int')
clasif_lr = pd.DataFrame(data=y_pred, columns=["pronl1"])
pd.value_counts(clasif_lr.pronl1)
final_p = pd.concat([datos_BBVA, clasif_lr], axis=1)

######
########
cvec = CountVectorizer()
lr = LogisticRegression()

vectorizer=cvec
n_features=1000
classifier=lr
ngram_range=(1, 2)
vectorizer.set_params(max_features=n_features, ngram_range=ngram_range)

pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
y_train=y_train.astype('int')       
sentiment_fit = pipeline.fit(x_train, y_train)
y_pred = sentiment_fit.predict(x_test_BBVA)
y_pred=y_pred.astype('int')
clasif_lr = pd.DataFrame(data=y_pred, columns=["pronl2"])
pd.value_counts(clasif_lr.pronl2)
final_p = pd.concat([final_p, clasif_lr], axis=1)
########
cvec = CountVectorizer()
lr = LogisticRegression()

vectorizer=cvec
n_features=1000
classifier=lr
ngram_range=(1, 3)
vectorizer.set_params(max_features=n_features, ngram_range=ngram_range)

pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
y_train=y_train.astype('int')       
sentiment_fit = pipeline.fit(x_train, y_train)
y_pred = sentiment_fit.predict(x_test_BBVA)
y_pred=y_pred.astype('int')
clasif_lr = pd.DataFrame(data=y_pred, columns=["pronl3"])
pd.value_counts(clasif_lr.pronl3)
final_p = pd.concat([final_p, clasif_lr], axis=1)
########
cvec = CountVectorizer()
ls = LinearSVC()

vectorizer=cvec
n_features=1000
classifier=ls
ngram_range=(1, 1)
vectorizer.set_params(max_features=n_features, ngram_range=ngram_range)

pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
y_train=y_train.astype('int')       
sentiment_fit = pipeline.fit(x_train, y_train)
y_pred = sentiment_fit.predict(x_test_BBVA)
y_pred=y_pred.astype('int')
clasif_lr = pd.DataFrame(data=y_pred, columns=["pronls1"])
pd.value_counts(clasif_lr.pronls1)
final_p = pd.concat([final_p, clasif_lr], axis=1)
########
cvec = CountVectorizer()
ls = LinearSVC()

vectorizer=cvec
n_features=1000
classifier=ls
ngram_range=(1, 2)
vectorizer.set_params(max_features=n_features, ngram_range=ngram_range)

pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
y_train=y_train.astype('int')       
sentiment_fit = pipeline.fit(x_train, y_train)
y_pred = sentiment_fit.predict(x_test_BBVA)
y_pred=y_pred.astype('int')
clasif_lr = pd.DataFrame(data=y_pred, columns=["pronls2"])
pd.value_counts(clasif_lr.pronls2)
final_p = pd.concat([final_p, clasif_lr], axis=1)
########
cvec = CountVectorizer()
ls = LinearSVC()

vectorizer=cvec
n_features=1000
classifier=ls
ngram_range=(1, 3)
vectorizer.set_params(max_features=n_features, ngram_range=ngram_range)

pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
y_train=y_train.astype('int')       
sentiment_fit = pipeline.fit(x_train, y_train)
y_pred = sentiment_fit.predict(x_test_BBVA)
y_pred=y_pred.astype('int')
clasif_lr = pd.DataFrame(data=y_pred, columns=["pronls3"])
pd.value_counts(clasif_lr.pronls3)
final_p = pd.concat([final_p, clasif_lr], axis=1)
########
cvec = CountVectorizer()
MNV = MultinomialNB()

vectorizer=cvec
n_features=1000
classifier=MNV
ngram_range=(1, 1)
vectorizer.set_params(max_features=n_features, ngram_range=ngram_range)

pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
y_train=y_train.astype('int')       
sentiment_fit = pipeline.fit(x_train, y_train)
y_pred = sentiment_fit.predict(x_test_BBVA)
y_pred=y_pred.astype('int')
clasif_lr = pd.DataFrame(data=y_pred, columns=["pronMNN1"])
pd.value_counts(clasif_lr.pronMNN1)
final_p = pd.concat([final_p, clasif_lr], axis=1)
########
cvec = CountVectorizer()
MNV = MultinomialNB()

vectorizer=cvec
n_features=1000
classifier=MNV
ngram_range=(1, 2)
vectorizer.set_params(max_features=n_features, ngram_range=ngram_range)

pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
y_train=y_train.astype('int')       
sentiment_fit = pipeline.fit(x_train, y_train)
y_pred = sentiment_fit.predict(x_test_BBVA)
y_pred=y_pred.astype('int')
clasif_lr = pd.DataFrame(data=y_pred, columns=["pronMNN2"])
pd.value_counts(clasif_lr.pronMNN2)
final_p = pd.concat([final_p, clasif_lr], axis=1)
########
cvec = CountVectorizer()
MNV = MultinomialNB()

vectorizer=cvec
n_features=1000
classifier=MNV
ngram_range=(1, 3)
vectorizer.set_params(max_features=n_features, ngram_range=ngram_range)

pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
y_train=y_train.astype('int')       
sentiment_fit = pipeline.fit(x_train, y_train)
y_pred = sentiment_fit.predict(x_test_BBVA)
y_pred=y_pred.astype('int')
clasif_lr = pd.DataFrame(data=y_pred, columns=["pronMNN3"])
pd.value_counts(clasif_lr.pronMNN3)
final_p = pd.concat([final_p, clasif_lr], axis=1)
########
cvec = CountVectorizer()
BNV = BernoulliNB()

vectorizer=cvec
n_features=1000
classifier=BNV
ngram_range=(1, 1)
vectorizer.set_params(max_features=n_features, ngram_range=ngram_range)

pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
y_train=y_train.astype('int')       
sentiment_fit = pipeline.fit(x_train, y_train)
y_pred = sentiment_fit.predict(x_test_BBVA)
y_pred=y_pred.astype('int')
clasif_lr = pd.DataFrame(data=y_pred, columns=["pronBNV1"])
print(pd.value_counts(clasif_lr.pronBNV1))
final_p = pd.concat([final_p, clasif_lr], axis=1)
########
cvec = CountVectorizer()
BNV = BernoulliNB()

vectorizer=cvec
n_features=1000
classifier=BNV
ngram_range=(1, 2)
vectorizer.set_params(max_features=n_features, ngram_range=ngram_range)

pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
y_train=y_train.astype('int')       
sentiment_fit = pipeline.fit(x_train, y_train)
y_pred = sentiment_fit.predict(x_test_BBVA)
y_pred=y_pred.astype('int')
clasif_lr = pd.DataFrame(data=y_pred, columns=["pronBNV2"])
print(pd.value_counts(clasif_lr.pronBNV2))
final_p = pd.concat([final_p, clasif_lr], axis=1)
########
cvec = CountVectorizer()
BNV = BernoulliNB()

vectorizer=cvec
n_features=1000
classifier=MNV
ngram_range=(1, 3)
vectorizer.set_params(max_features=n_features, ngram_range=ngram_range)

pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
y_train=y_train.astype('int')       
sentiment_fit = pipeline.fit(x_train, y_train)
y_pred = sentiment_fit.predict(x_test_BBVA)
y_pred=y_pred.astype('int')
clasif_lr = pd.DataFrame(data=y_pred, columns=["pronBNV3"])
print(pd.value_counts(clasif_lr.pronBNV3))
final_p = pd.concat([final_p, clasif_lr], axis=1)
###
##
########
cvec = CountVectorizer()
RC = RidgeClassifier()

vectorizer=cvec
n_features=1000
classifier=RC
ngram_range=(1, 1)
vectorizer.set_params(max_features=n_features, ngram_range=ngram_range)

pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
y_train=y_train.astype('int')       
sentiment_fit = pipeline.fit(x_train, y_train)
y_pred = sentiment_fit.predict(x_test_BBVA)
y_pred=y_pred.astype('int')
clasif_lr = pd.DataFrame(data=y_pred, columns=["pronRC1"])
print(pd.value_counts(clasif_lr.pronRC1))
final_p = pd.concat([final_p, clasif_lr], axis=1)
#####
cvec = CountVectorizer()
RC = RidgeClassifier()

vectorizer=cvec
n_features=1000
classifier=RC
ngram_range=(1, 2)
vectorizer.set_params(max_features=n_features, ngram_range=ngram_range)

pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
y_train=y_train.astype('int')       
sentiment_fit = pipeline.fit(x_train, y_train)
y_pred = sentiment_fit.predict(x_test_BBVA)
y_pred=y_pred.astype('int')
clasif_lr = pd.DataFrame(data=y_pred, columns=["pronRC2"])
print(pd.value_counts(clasif_lr.pronRC2))
final_p = pd.concat([final_p, clasif_lr], axis=1)
#####
cvec = CountVectorizer()
RC = RidgeClassifier()

vectorizer=cvec
n_features=1000
classifier=RC
ngram_range=(1, 3)
vectorizer.set_params(max_features=n_features, ngram_range=ngram_range)

pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
y_train=y_train.astype('int')       
sentiment_fit = pipeline.fit(x_train, y_train)
y_pred = sentiment_fit.predict(x_test_BBVA)
y_pred=y_pred.astype('int')
clasif_lr = pd.DataFrame(data=y_pred, columns=["pronRC3"])
print(pd.value_counts(clasif_lr.pronRC3))
final_p = pd.concat([final_p, clasif_lr], axis=1)
#####
####
##
cvec = CountVectorizer()
AB = AdaBoostClassifier()

vectorizer=cvec
n_features=1000
classifier=AB
ngram_range=(1, 1)
vectorizer.set_params(max_features=n_features, ngram_range=ngram_range)

pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
y_train=y_train.astype('int')       
sentiment_fit = pipeline.fit(x_train, y_train)
y_pred = sentiment_fit.predict(x_test_BBVA)
y_pred=y_pred.astype('int')
clasif_lr = pd.DataFrame(data=y_pred, columns=["pronAB1"])
print(pd.value_counts(clasif_lr.pronAB1))
final_p = pd.concat([final_p, clasif_lr], axis=1)
##
cvec = CountVectorizer()
AB = AdaBoostClassifier()

vectorizer=cvec
n_features=1000
classifier=AB
ngram_range=(1, 2)
vectorizer.set_params(max_features=n_features, ngram_range=ngram_range)

pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
y_train=y_train.astype('int')       
sentiment_fit = pipeline.fit(x_train, y_train)
y_pred = sentiment_fit.predict(x_test_BBVA)
y_pred=y_pred.astype('int')
clasif_lr = pd.DataFrame(data=y_pred, columns=["pronAB2"])
print(pd.value_counts(clasif_lr.pronAB2))
final_p = pd.concat([final_p, clasif_lr], axis=1)
##
cvec = CountVectorizer()
AB = AdaBoostClassifier()

vectorizer=cvec
n_features=1000
classifier=AB
ngram_range=(1, 3)
vectorizer.set_params(max_features=n_features, ngram_range=ngram_range)

pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
y_train=y_train.astype('int')       
sentiment_fit = pipeline.fit(x_train, y_train)
y_pred = sentiment_fit.predict(x_test_BBVA)
y_pred=y_pred.astype('int')
clasif_lr = pd.DataFrame(data=y_pred, columns=["pronAB3"])
print(pd.value_counts(clasif_lr.pronAB3))
final_p = pd.concat([final_p, clasif_lr], axis=1)
##
cvec = CountVectorizer()
P = Perceptron()

vectorizer=cvec
n_features=1000
classifier=P
ngram_range=(1, 1)
vectorizer.set_params(max_features=n_features, ngram_range=ngram_range)

pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
y_train=y_train.astype('int')       
sentiment_fit = pipeline.fit(x_train, y_train)
y_pred = sentiment_fit.predict(x_test_BBVA)
y_pred=y_pred.astype('int')
clasif_lr = pd.DataFrame(data=y_pred, columns=["pronP1"])
print(pd.value_counts(clasif_lr.pronP1))
final_p = pd.concat([final_p, clasif_lr], axis=1)
####
##
cvec = CountVectorizer()
P = Perceptron()

vectorizer=cvec
n_features=1000
classifier=P
ngram_range=(1, 2)
vectorizer.set_params(max_features=n_features, ngram_range=ngram_range)

pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
y_train=y_train.astype('int')       
sentiment_fit = pipeline.fit(x_train, y_train)
y_pred = sentiment_fit.predict(x_test_BBVA)
y_pred=y_pred.astype('int')
clasif_lr = pd.DataFrame(data=y_pred, columns=["pronP2"])
print(pd.value_counts(clasif_lr.pronP2))
final_p = pd.concat([final_p, clasif_lr], axis=1)
####
###
##
cvec = CountVectorizer()
P = Perceptron()

vectorizer=cvec
n_features=1000
classifier=P
ngram_range=(1, 3)
vectorizer.set_params(max_features=n_features, ngram_range=ngram_range)

pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
y_train=y_train.astype('int')       
sentiment_fit = pipeline.fit(x_train, y_train)
y_pred = sentiment_fit.predict(x_test_BBVA)
y_pred=y_pred.astype('int')
clasif_lr = pd.DataFrame(data=y_pred, columns=["pronP3"])
print(pd.value_counts(clasif_lr.pronP3))
final_p = pd.concat([final_p, clasif_lr], axis=1)
###
##
cvec = CountVectorizer()
PA = PassiveAggressiveClassifier()

vectorizer=cvec
n_features=1000
classifier=PA
ngram_range=(1, 1)
vectorizer.set_params(max_features=n_features, ngram_range=ngram_range)

pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
y_train=y_train.astype('int')       
sentiment_fit = pipeline.fit(x_train, y_train)
y_pred = sentiment_fit.predict(x_test_BBVA)
y_pred=y_pred.astype('int')
clasif_lr = pd.DataFrame(data=y_pred, columns=["pronPA1"])
print(pd.value_counts(clasif_lr.pronPA1))
final_p = pd.concat([final_p, clasif_lr], axis=1)
##
cvec = CountVectorizer()
PA = PassiveAggressiveClassifier()

vectorizer=cvec
n_features=1000
classifier=PA
ngram_range=(1, 2)
vectorizer.set_params(max_features=n_features, ngram_range=ngram_range)

pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
y_train=y_train.astype('int')       
sentiment_fit = pipeline.fit(x_train, y_train)
y_pred = sentiment_fit.predict(x_test_BBVA)
y_pred=y_pred.astype('int')
clasif_lr = pd.DataFrame(data=y_pred, columns=["pronPA2"])
print(pd.value_counts(clasif_lr.pronPA2))
final_p = pd.concat([final_p, clasif_lr], axis=1)
##
cvec = CountVectorizer()
PA = PassiveAggressiveClassifier()

vectorizer=cvec
n_features=1000
classifier=PA
ngram_range=(1, 3)
vectorizer.set_params(max_features=n_features, ngram_range=ngram_range)

pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
y_train=y_train.astype('int')       
sentiment_fit = pipeline.fit(x_train, y_train)
y_pred = sentiment_fit.predict(x_test_BBVA)
y_pred=y_pred.astype('int')
clasif_lr = pd.DataFrame(data=y_pred, columns=["pronPA3"])
print(pd.value_counts(clasif_lr.pronPA3))
final_p = pd.concat([final_p, clasif_lr], axis=1)
######
##
cvec = CountVectorizer()
NC = NearestCentroid()

vectorizer=cvec
n_features=1000
classifier=NC
ngram_range=(1, 1)
vectorizer.set_params(max_features=n_features, ngram_range=ngram_range)

pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
y_train=y_train.astype('int')       
sentiment_fit = pipeline.fit(x_train, y_train)
y_pred = sentiment_fit.predict(x_test_BBVA)
y_pred=y_pred.astype('int')
clasif_lr = pd.DataFrame(data=y_pred, columns=["pronNC1"])
print(pd.value_counts(clasif_lr.pronNC1))
final_p = pd.concat([final_p, clasif_lr], axis=1)
##
cvec = CountVectorizer()
NC = NearestCentroid()

vectorizer=cvec
n_features=1000
classifier=NC
ngram_range=(1, 2)
vectorizer.set_params(max_features=n_features, ngram_range=ngram_range)

pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
y_train=y_train.astype('int')       
sentiment_fit = pipeline.fit(x_train, y_train)
y_pred = sentiment_fit.predict(x_test_BBVA)
y_pred=y_pred.astype('int')
clasif_lr = pd.DataFrame(data=y_pred, columns=["pronNC2"])
print(pd.value_counts(clasif_lr.pronNC2))
final_p = pd.concat([final_p, clasif_lr], axis=1)
###
##
cvec = CountVectorizer()
NC = NearestCentroid()

vectorizer=cvec
n_features=1000
classifier=NC
ngram_range=(1, 3)
vectorizer.set_params(max_features=n_features, ngram_range=ngram_range)

pipeline = Pipeline([
            ('vectorizer', vectorizer),
            ('classifier', classifier)
        ])
y_train=y_train.astype('int')       
sentiment_fit = pipeline.fit(x_train, y_train)
y_pred = sentiment_fit.predict(x_test_BBVA)
y_pred=y_pred.astype('int')
clasif_lr = pd.DataFrame(data=y_pred, columns=["pronNC3"])
print(pd.value_counts(clasif_lr.pronNC3))
final_p = pd.concat([final_p, clasif_lr], axis=1)
file_guardar=directorio+"/pronostico/modelo.csv"
print("Se genera la base de datos clasificada con el mejor modelo")
print("Se guarda en:", file_guardar)
final_p.to_csv(file_guardar, sep=',', encoding='utf-8', index=False)

