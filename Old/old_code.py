
#LLibrearias iniciales
import re
import math
import numpy as np
import pandas as pd

#
import xml.etree.ElementTree as et 
from collections import Counter
import itertools
import collections

#Procesamiento Natural
import nltk
from nltk.collocations import BigramCollocationFinder, BigramAssocMeasures
from nltk.corpus import stopwords
from nltk import word_tokenize 
from nltk.stem import WordNetLemmatizer
from nltk.util import ngrams
from nltk import bigrams

#Descargamos algunos objetos que nos ayudaran al analsis de sentimietos
nltk.download('stopwords')
nltk.download('punkt')
stop_words = set(stopwords.words('spanish'))

#Graficas
import matplotlib.pyplot as plt

'''
clean_data:
    data: Acepta una serie Pandas
'''



n_gram:
    |---- data:

Word_Cloud:
    |---- texto:

#Limpieza de datos
def clean_data(data):
        
    data=data.apply(lambda x: x.astype(str).str.lower())
    data["message"]=data["message"].str.replace("año", "anio", regex=False)
    data["message"]=data["message"].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
    data["message"]=data["message"].str.replace("!", "", regex=False)
    data["message"]=data["message"].str.replace("¡", "", regex=False)
    data["message"]=data["message"].str.replace("?", "", regex=False)
    data["message"]=data["message"].str.replace("¿", "", regex=False)
    data["message"]=data["message"].str.replace(",", "", regex=False)
    data["message"]=data["message"].str.replace(".", "", regex=False)
    data["message"]=data["message"].str.replace(":)", "", regex=False)
    data["message"]=data["message"].str.replace("-", "", regex=False)
    data["message"]=data["message"].str.replace("'", "", regex=False)
    data["message"]=data["message"].str.replace("\"", " ", regex=False)
    data["message"]=data["message"].str.replace(":", "", regex=False)
    data["message"]=data["message"].str.replace(":(", "", regex=False)
    data["message"]=data["message"].str.replace(r'4', "", regex=False)
    data["message"]=data["message"].str.replace(")", " ", regex=False)
    data["message"]=data["message"].str.replace("(", " ", regex=False)
    data["message"]=data["message"].str.replace(";"," ", regex=False)
    data["message"]=data["message"].str.replace(">"," ", regex=False)
    data["message"]=data["message"].str.replace("@[^s]+", "", regex=True)
    data["message"]=data["message"].str.replace("httpS+", "", regex=True)
    data["message"]=data["message"].str.replace("https//", "", regex=True)
    data["message"]=data["message"].str.replace("buenos", "", regex=False)
    data["message"]=data["message"].str.replace("buenas", "", regex=False)
    data["message"]=data["message"].str.replace("dias", "", regex=False)
    data["message"]=data["message"].str.replace("tardes", "", regex=False)
    data["message"]=data["message"].str.replace("hola", "", regex=False)
      
    return data



def n_gram(data):

    #Guardams los mensajes en una lista
    lista = data["message"].values.tolist()
    #Tokenizar 
    tweets_t = [word_tokenize(tweet) for tweet in lista]

    tweets_nsw = [[word for word in tweet_words if not word in stop_words]
                  for tweet_words in tweets_t]
    #Lista sin vacios
    tweets_nsw = list(filter(None, tweets_nsw))
    #Crear bigramas, se varia el dos.
    terms_bigram = [list(ngrams(w_tweet,2)) for w_tweet in tweets_nsw]
    ###
    bigrams = list(itertools.chain(*terms_bigram))
    # Crear el objeto con los bogramas contados
    bigram_counts = collections.Counter(bigrams)
    ##ver 10 primeros bigramas
    bigram_counts.most_common(100)
    ###
    palabras = list(itertools.chain(*tweets_nsw))
    contador=Counter(palabras)

    ###
    c=dict(bigram_counts)
    key=list(c.keys())
    value=list(c.values())
    list1, list2 = zip(*key)
    ##
    c1=dict(contador)
    suma_uni=sum(list(c1.values()))
    ########
    list3=[]
    list4=[]
    ###
    for i in range(0,len(list1)):
       list3.append(c1.get(list1[i]))
    for i in range(0,len(list2)):
       list4.append(c1.get(list2[i]))
    #####

    df1 = pd.DataFrame(list(list1), columns=['palabra1'])
    df1["palabra2"]=pd.DataFrame(list(list2))
    df1["fx"]=pd.DataFrame(list(list3))
    df1["fy"]=pd.DataFrame(list(list4))
    df1["fxy"]=pd.DataFrame(list(value))
    suma_bi=sum(df1["fxy"])
    df1["pfx"]=df1["fx"]/suma_uni
    df1["pfy"]=df1["fy"]/suma_uni
    df1["pfxy"]=df1["fxy"]/suma_bi
    df1["Ixy"]=df1["pfxy"]/(df1["pfx"]*df1["pfy"])
    df1=df1[df1["fxy"]>=5]
    df1["Ixy"]=np.log(df1["Ixy"])
    df1=df1.sort_values('fxy', ascending=False)
    df1.reset_index(drop=True, inplace=True)
    
    return df1


def Word_Cloud(texto):

    lista = list(texto.message)
    ###
    tweets_t = [word_tokenize(tweet) for tweet in lista]
    ########
    tweets_nsw = [[word for word in tweet_words if not word in stop_words]
                  for tweet_words in tweets_t]
    #####
    #lista sin vacíos
    tweets_nsw=[" ".join(x) for x in tweets_nsw]
    texto2 = pd.DataFrame(tweets_nsw,columns=['texto2'])
    texto = pd.concat([texto, texto2], axis=1)

    neg_tweets = texto[texto.sentimiento == -1]
    neg_string = []
    for t in neg_tweets.texto2:
        neg_string.append(t)
    neg_string = pd.Series(neg_string).str.cat(sep=' ')
    from wordcloud import WordCloud

    wordcloud = WordCloud(width=1600, height=800,max_font_size=200).generate(neg_string)
    plt.figure(figsize=(12,10))
    plt.imshow(wordcloud, interpolation="bilinear")
    plt.axis("off")
    plt.show()

    pos_tweets = texto[texto.sentimiento == 1]
    pos_string = []
    for t in pos_tweets.texto2:
        pos_string.append(t)
    pos_string = pd.Series(pos_string).str.cat(sep=' ')

    wordcloud = WordCloud(width=1600, height=800,max_font_size=200).generate(pos_string)
    plt.figure(figsize=(12,10))
    plt.imshow(wordcloud, interpolation="bilinear")
    plt.axis("off")
    plt.show()
    texto=texto[(texto["sentimiento"]==1) | (texto["sentimiento"]==-1)]
    texto=texto.sort_values(by=['sentimiento'], ascending=True)
    texto.reset_index(drop=True, inplace=True)
    
    return texto