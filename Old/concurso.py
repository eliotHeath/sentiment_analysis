#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 16 01:34:37 2020

@author: francisco
"""
import xml.etree.ElementTree as et 
from lxml import objectify
from pandas import DataFrame
import matplotlib
######
import nltk
from nltk.collocations import BigramCollocationFinder, BigramAssocMeasures
from collections import Counter
from nltk.corpus import stopwords
####
from nltk import word_tokenize 
from nltk.stem import WordNetLemmatizer
from nltk.util import ngrams
from nltk import bigrams
import math
import numpy as np
import pandas as pd
import itertools
import collections
nltk.download('stopwords')
stop_words = set(stopwords.words('spanish'))
import matplotlib.pyplot as plt
############



xtree = et.parse("/home/francisco/Documentos/Personal/BBVA/Español/mexico/TASS2019_country_MX_train.xml")
xroot = xtree.getroot()

texto=pd.DataFrame(columns=["texto","sentimiento"])
for node in xroot: 
    #print(node)
    s_tweet = node.find("content").text
    s_sentiment = node[5][0][0].text
    df={'texto': [s_tweet],
        'sentimiento': [s_sentiment]}
    df=pd.DataFrame(df, columns=["texto","sentimiento"])
    texto=pd.concat([texto,df])
texto.reset_index(drop=True, inplace=True)
texto1=texto
texto=texto.apply(lambda x: x.astype(str).str.lower())
texto["texto"]=texto["texto"].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
texto["texto"]=texto["texto"].str.replace("!", "", regex=False)
texto["texto"]=texto["texto"].str.replace("¡", "", regex=False)
texto["texto"]=texto["texto"].str.replace(":)", "", regex=False)
texto["texto"]=texto["texto"].str.replace(":(", "", regex=False)
texto["texto"]=texto["texto"].str.replace("?", "", regex=False)
texto["texto"]=texto["texto"].str.replace("¿", "", regex=False)
texto["texto"]=texto["texto"].str.replace("-", "", regex=False)
texto["texto"]=texto["texto"].str.replace(".", " ", regex=False)
texto["texto"]=texto["texto"].str.replace(",", " ", regex=False)
texto["texto"]=texto["texto"].str.replace("'", "", regex=False)
texto["texto"]=texto["texto"].str.replace("\"", " ", regex=False)
texto["texto"]=texto["texto"].str.replace(")", " ", regex=False)
texto["texto"]=texto["texto"].str.replace("(", " ", regex=False)
texto["texto"]=texto["texto"].str.replace(";"," ", regex=False)
texto["texto"]=texto["texto"].str.replace(">"," ", regex=False)
texto["texto"]=texto["texto"].str.replace("@[^s]+", "", regex=True)
texto["texto"]=texto["texto"].str.replace("httpS+", "", regex=True)
texto=texto.astype(str).apply(lambda x: x.str.encode('ascii', 'ignore').str.decode('ascii'))

lista = texto["texto"].values.tolist()
###
tweets_t = [word_tokenize(tweet) for tweet in lista]
########
tweets_nsw = [[word for word in tweet_words if not word in stop_words]
              for tweet_words in tweets_t]
#####
#lista sin vacíos
#tweets_nsw = list(filter(None, tweets_nsw))
#Crear bigramas, se varia el dos.
tweets_nsw=[" ".join(x) for x in tweets_nsw]
texto2 = DataFrame(tweets_nsw,columns=['texto2'])
texto = pd.concat([texto, texto2], axis=1)


neg_tweets = texto[texto.sentimiento == "n"]
neg_string = []
for t in neg_tweets.texto2:
    neg_string.append(t)
neg_string = pd.Series(neg_string).str.cat(sep=' ')
from wordcloud import WordCloud

wordcloud = WordCloud(width=1600, height=800,max_font_size=200).generate(neg_string)
plt.figure(figsize=(12,10))
plt.imshow(wordcloud, interpolation="bilinear")
plt.axis("off")
plt.show()

pos_tweets = texto[texto.sentimiento == "p"]
pos_string = []
for t in pos_tweets.texto2:
    pos_string.append(t)
pos_string = pd.Series(pos_string).str.cat(sep=' ')

wordcloud = WordCloud(width=1600, height=800,max_font_size=200).generate(pos_string)
plt.figure(figsize=(12,10))
plt.imshow(wordcloud, interpolation="bilinear")
plt.axis("off")
plt.show()
texto=texto[(texto["sentimiento"]=='p') | (texto["sentimiento"]=='n')]
texto=texto.sort_values(by=['sentimiento'], ascending=True)
texto.reset_index(drop=True, inplace=True)

#########
import sklearn
from sklearn.feature_extraction.text import CountVectorizer
cvec = CountVectorizer()
cvec.fit(texto.texto2)
len(cvec.get_feature_names())

#########
neg_doc_matrix = cvec.transform(texto[texto.sentimiento == "n"].texto2)
pos_doc_matrix = cvec.transform(texto[texto.sentimiento == "p"].texto2)
neg_tf = np.sum(neg_doc_matrix,axis=0)
pos_tf = np.sum(pos_doc_matrix,axis=0)
neg = np.squeeze(np.asarray(neg_tf))
pos = np.squeeze(np.asarray(pos_tf))
term_freq_df = pd.DataFrame([neg,pos],columns=cvec.get_feature_names()).transpose()
########
document_matrix = cvec.transform(texto.texto2)
texto[texto.sentimiento == "n"].tail()
########
neg_batches = np.linspace(0,504,100).astype(int)
i=0
neg_tf = []
while i < len(neg_batches)-1:
    batch_result = np.sum(document_matrix[neg_batches[i]:neg_batches[i+1]].toarray(),axis=0)
    neg_tf.append(batch_result)
    if (i % 10 == 0) | (i == len(neg_batches)-2):
        print(neg_batches[i+1],"entries' term freuquency calculated")
    i += 1
##
pos_batches = np.linspace(504,816,100).astype(int)
i=0
pos_tf = []
while i < len(pos_batches)-1:
    batch_result = np.sum(document_matrix[pos_batches[i]:pos_batches[i+1]].toarray(),axis=0)
    pos_tf.append(batch_result)
    if (i % 10 == 0) | (i == len(pos_batches)-2):
        print(pos_batches[i+1],"entries' term freuquency calculated")
    i += 1
#######
neg = np.sum(neg_tf,axis=0)
pos = np.sum(pos_tf,axis=0)
term_freq_df2 = pd.DataFrame([neg,pos],columns=cvec.get_feature_names()).transpose()
term_freq_df2.columns = ['negative', 'positive']
term_freq_df2['total'] = term_freq_df2['negative'] + term_freq_df2['positive']
term_freq_df2.sort_values(by='total', ascending=False).iloc[:10]
######
y_pos = np.arange(50)
plt.figure(figsize=(12,10))
plt.bar(y_pos, term_freq_df2.sort_values(by='negative', ascending=False)['negative'][:50], align='center', alpha=0.5)
plt.xticks(y_pos, term_freq_df2.sort_values(by='negative', ascending=False)['negative'][:50].index,rotation='vertical')
plt.ylabel('Frequency')
plt.xlabel('Top 50 negative tokens')
plt.title('Top 50 tokens in negative tweets')
####
y_pos = np.arange(50)
plt.figure(figsize=(12,10))
plt.bar(y_pos, term_freq_df2.sort_values(by='positive', ascending=False)['positive'][:50], align='center', alpha=0.5)
plt.xticks(y_pos, term_freq_df2.sort_values(by='positive', ascending=False)['positive'][:50].index,rotation='vertical')
plt.ylabel('Frequency')
plt.xlabel('Top 50 positive tokens')
plt.title('Top 50 tokens in positive tweets')
#####
import seaborn as sns
plt.figure(figsize=(8,6))
ax = sns.regplot(x="negative", y="positive",fit_reg=False, scatter_kws={'alpha':0.5},data=term_freq_df2)
plt.ylabel('Positive Frequency')
plt.xlabel('Negative Frequency')
plt.title('Negative Frequency vs Positive Frequency')
#########
term_freq_df2['pos_rate'] = term_freq_df2['positive'] * 1./term_freq_df2['total']
term_freq_df2.sort_values(by='pos_rate', ascending=False).iloc[:10]
########
term_freq_df2['pos_freq_pct'] = term_freq_df2['positive'] * 1./term_freq_df2['positive'].sum()
term_freq_df2.sort_values(by='pos_freq_pct', ascending=False).iloc[:10]
####
from scipy.stats import hmean

term_freq_df2['pos_hmean'] = term_freq_df2.apply(lambda x: (hmean([x['pos_rate'], x['pos_freq_pct']])
                                                                   if x['pos_rate'] > 0 and x['pos_freq_pct'] > 0 
                                                                   else 0), axis=1)                                                        
term_freq_df2.sort_values(by='pos_hmean', ascending=False).iloc[:10]
############
from scipy.stats import norm
def normcdf(x):
    return norm.cdf(x, x.mean(), x.std())

term_freq_df2['pos_rate_normcdf'] = normcdf(term_freq_df2['pos_rate'])
term_freq_df2['pos_freq_pct_normcdf'] = normcdf(term_freq_df2['pos_freq_pct'])
####
term_freq_df2['pos_rate_normcdf'].fillna(.001, inplace = True)
term_freq_df2['pos_freq_pct_normcdf'].fillna(.001, inplace = True)
#####
term_freq_df2['pos_normcdf_hmean'] = hmean([term_freq_df2['pos_rate_normcdf'], term_freq_df2['pos_freq_pct_normcdf']])
term_freq_df2.sort_values(by='pos_normcdf_hmean', ascending=False).iloc[:10]
#####################
term_freq_df2['neg_rate'] = term_freq_df2['negative'] * 1./term_freq_df2['total']
term_freq_df2['neg_freq_pct'] = term_freq_df2['negative'] * 1./term_freq_df2['negative'].sum()

term_freq_df2['neg_hmean'] = term_freq_df2.apply(lambda x: (hmean([x['neg_rate'], x['neg_freq_pct']])
                                                                   if x['neg_rate'] > 0 and x['neg_freq_pct'] > 0 
                                                                   else 0), axis=1)                                                        
term_freq_df2['neg_rate_normcdf'] = normcdf(term_freq_df2['neg_rate'])
term_freq_df2['neg_freq_pct_normcdf'] = normcdf(term_freq_df2['neg_freq_pct'])
term_freq_df2['neg_rate_normcdf'].fillna(.001, inplace = True)
term_freq_df2['neg_freq_pct_normcdf'].fillna(.001, inplace = True)
term_freq_df2['neg_normcdf_hmean'] = hmean([term_freq_df2['neg_rate_normcdf'], term_freq_df2['neg_freq_pct_normcdf']])
term_freq_df2.sort_values(by='neg_normcdf_hmean', ascending=False).iloc[:10]
#####
plt.figure(figsize=(8,6))
ax = sns.regplot(x="neg_hmean", y="pos_hmean",fit_reg=False, scatter_kws={'alpha':0.5},data=term_freq_df2)
plt.ylabel('Positive Rate and Frequency Harmonic Mean')
plt.xlabel('Negative Rate and Frequency Harmonic Mean')
plt.title('neg_hmean vs pos_hmean')
#####
plt.figure(figsize=(8,6))
ax = sns.regplot(x="neg_normcdf_hmean", y="pos_normcdf_hmean",fit_reg=False, scatter_kws={'alpha':0.5},data=term_freq_df2)
plt.ylabel('Positive Rate and Frequency CDF Harmonic Mean')
plt.xlabel('Negative Rate and Frequency CDF Harmonic Mean')
plt.title('neg_normcdf_hmean vs pos_normcdf_hmean')
########
#######
from bokeh.plotting import figure
from bokeh.io import output_notebook, show
from bokeh.models import LinearColorMapper
output_notebook()
color_mapper = LinearColorMapper(palette='Inferno256', low=min(term_freq_df2.pos_normcdf_hmean), high=max(term_freq_df2.pos_normcdf_hmean))
p = figure(x_axis_label='neg_normcdf_hmean', y_axis_label='pos_normcdf_hmean')
p.circle('neg_normcdf_hmean','pos_normcdf_hmean',size=5,alpha=0.3,source=term_freq_df2,color={'field': 'pos_normcdf_hmean', 'transform': color_mapper})
from bokeh.models import HoverTool
hover = HoverTool(tooltips=[('token','@index')])
p.add_tools(hover)
show(p)
#######
######
texto['sentimiento2']=texto['sentimiento']
texto.sentimiento2[texto.sentimiento == 'p'] = 1 
texto.sentimiento2[texto.sentimiento == 'n'] = 0 
x = texto.texto2
y = texto.sentimiento2
from sklearn.model_selection import train_test_split
SEED = 2000
x_train, x_validation_and_test, y_train, y_validation_and_test = train_test_split(x, y, test_size=.2, random_state=SEED)
x_validation, x_test, y_validation, y_test = train_test_split(x_validation_and_test, y_validation_and_test, test_size=.5, random_state=SEED)
print("Train set has total {0} entries with {1:.2f}% negative, {2:.2f}% positive".format(len(x_train),(len(x_train[y_train == 1]) / (len(x_train)*1.))*100,(len(x_train[y_train == 0]) / (len(x_train)*1.))*100))
print("Validation set has total {0} entries with {1:.2f}% negative, {2:.2f}% positive".format(len(x_validation), (len(x_validation[y_validation == 1]) / (len(x_validation)*1.))*100, (len(x_validation[y_validation == 0]) / (len(x_validation)*1.))*100))
print ("Test set has total {0} entries with {1:.2f}% negative, {2:.2f}% positive".format(len(x_test), (len(x_test[y_test == 1]) / (len(x_test)*1.))*100,(len(x_test[y_test == 0]) / (len(x_test)*1.))*100))
##############
from textblob import TextBlob
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report, confusion_matrix
tbresult = [TextBlob(i).sentiment.polarity for i in x_validation]
tbpred = [0 if n < 0 else 1 for n in tbresult]
y_validation.reset_index(drop=True, inplace=True)
y_validation=y_validation.values.tolist()
conmat = np.array(confusion_matrix(y_validation, tbpred, labels=[1,0]))
confusion = pd.DataFrame(conmat, index=['positive', 'negative'],
                         columns=['predicted_positive','predicted_negative'])
print("Accuracy Score: {0:.2f}%".format(accuracy_score(y_validation, tbpred)*100))
print("-"*80)
print("Confusion Matrix\n")
print(confusion)
print("-"*80)
print("Classification Report\n")
print(classification_report(y_validation, tbpred))