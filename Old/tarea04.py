#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 30 09:49:56 2020

@author: francisco
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 19 00:19:26 2020

@author: francisco
"""
###Importar las librerías
import nltk
from nltk.collocations import BigramCollocationFinder, BigramAssocMeasures
from collections import Counter
from nltk.corpus import stopwords
####
from nltk import word_tokenize 
from nltk.stem import WordNetLemmatizer
from nltk.util import ngrams
from nltk import bigrams
import math
import numpy as np
import pandas as pd
import itertools
import collections
nltk.download('stopwords')
stop_words = set(stopwords.words('spanish'))
#########

file_path="/home/francisco/Documentos/Personal/BBVA/ComentariosBBVA_010120-310820_V001.csv"
data1=df = pd.read_csv(file_path)
data=data1.apply(lambda x: x.astype(str).str.lower())
data["message"]=data["message"].str.replace("año", "anio", regex=False)
data["message"]=data["message"].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
data["message"]=data["message"].str.replace("!", "", regex=False)
data["message"]=data["message"].str.replace("¡", "", regex=False)
data["message"]=data["message"].str.replace(":)", "", regex=False)
data["message"]=data["message"].str.replace(":(", "", regex=False)
######borrar saludos
data["message"]=data["message"].str.replace("buenos", "", regex=False)
data["message"]=data["message"].str.replace("buenas", "", regex=False)
data["message"]=data["message"].str.replace("dias", "", regex=False)
data["message"]=data["message"].str.replace("tardes", "", regex=False)
data["message"]=data["message"].str.replace("hola", "", regex=False)
#bots
data= data[~data.message.str.contains("33756888034")]
data= data[~data.message.str.contains("podcast")]
data= data[~data.message.str.contains("viva")]
data= data[~data.message.str.contains("5554008913")]
data= data[~data.message.str.contains("suspendemos")]
data=data.drop_duplicates(subset='message', keep="last")
####
data["message"]=data["message"].str.replace("@[^\s]+", "", regex=True)
data["message"]=data["message"].str.replace("http\S+", "", regex=True)
data["message"]=data["message"].str.replace("[^\w\s]", "", regex=True)
data["message"]=data["message"].replace("nan", np.NAN, regex=False)
data["message"]=data["message"].replace(r'^\s*$', np.NAN, regex=True)
data=data.dropna(subset=["message"])
data=data.astype(str).apply(lambda x: x.str.encode('ascii', 'ignore').str.decode('ascii'))

####
#list = data["text"].iloc[1:1000].values.tolist()
lista = data["message"].values.tolist()
####
####
tweets_t = [word_tokenize(tweet) for tweet in lista]
########
tweets_nsw = [[word for word in tweet_words if not word in stop_words]
              for tweet_words in tweets_t]
#####
#lista sin vacíos
tweets_nsw = list(filter(None, tweets_nsw))
#Crear bigramas, se varia el dos.
terms_bigram = [list(ngrams(w_tweet,2)) for w_tweet in tweets_nsw]
###
bigrams = list(itertools.chain(*terms_bigram))

# Crear el objeto con los bogramas contados
bigram_counts = collections.Counter(bigrams)
##ver 10 primeros bigramas
bigram_counts.most_common(100)
###
palabras = list(itertools.chain(*tweets_nsw))
contador=Counter(palabras)

###
c=dict(bigram_counts)
key=list(c.keys())
value=list(c.values())
list1, list2 = zip(*key)
##
c1=dict(contador)
suma=sum(list(c1.values()))
########
list3=[]
list4=[]
###
for i in range(0,len(list1)):
   list3.append(c1.get(list1[i]))
for i in range(0,len(list2)):
   list4.append(c1.get(list2[i]))

df1 = pd.DataFrame(list(list1), columns=['palabra1'])
df1["palabra2"]=pd.DataFrame(list(list2))
df1["fx"]=pd.DataFrame(list(list3))
df1["fy"]=pd.DataFrame(list(list4))
df1["fxy"]=pd.DataFrame(list(value))
df1["pfx"]=df1["fx"]/suma
df1["pfy"]=df1["fy"]/suma
df1["pfxy"]=df1["fxy"]/suma
df1["Ixy"]=df1["pfxy"]/(df1["pfx"]*df1["pfy"])
df1["Ixy"]=np.log(df1["Ixy"])
df1=df1.sort_values('fxy', ascending=False)
df1.reset_index(drop=True, inplace=True)

data.to_csv("/home/francisco/Documentos/Miguel/bbva/inflimpio.csv", sep=',', encoding='utf-8')

