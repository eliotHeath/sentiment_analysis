import dash
import dash_html_components as html
import dash_core_components as dcc
import plotly.graph_objects as go
import pandas as pd
from dash.dependencies import Input, Output
import plotly.express as px

import utils.dash_reusable_components as drc

colors = {
    'background': '#31302F',
    'text': '#7FDBFF'
}


# Load data
df = pd.read_csv('data/numero.csv', parse_dates=True)
df['fecha'] = pd.to_datetime(df['fecha'],format = "%d/%m/%y")
df.sort_values(by=['fecha'], inplace=True)

df_p1 = pd.read_csv("data/p1.csv", parse_dates = True)
df_p1["pronP1"] =df_p1['pronP1'].str.replace(r"%",r".0").astype("float")/100
df_p1['fecha'] = pd.to_datetime(df_p1['fecha'], format = "%d/%m/%Y")
df_p1.sort_values(by=['fecha'], inplace=True)

#df_p1_plot = px.data.df_p1
fig = px.line(data_frame = df_p1, x = "fecha", y = ["Trend","mid", "pronP1"])
fig.update_layout(
    plot_bgcolor=colors['background'],
    paper_bgcolor=colors['background'],
    font_color=colors['text'],
    title={'text': 'Índice del modelo del  perceptrón', 'font': {'color': 'white'}, 'x': 0.5},
    xaxis={'range': [df_p1['fecha'].min(), df_p1['fecha'].max()]},
)

# Initialize the app
app = dash.Dash(__name__)
server = app.server
app.config.suppress_callback_exceptions = True

app.layout = html.Div(
            id="body",
            className="container scalable",
            children=[
                html.Div(
                    id="app-container",
                    # className="row",
                    children=[
                        html.Div(
                            # className="three columns",
                            id="left-column",
                            children=[
                                drc.Card(
                                    id="first-card",
                                    children=[
                                        html.H1("HORER"),
                                        html.P("Una herramienta para analizar las emociones de los clientes de BBVA"),
                                    ],
                                ),
                                drc.Card(
                                    id = "second-card",
                                    children=[
                                        drc.NamedDropdown(
                                            name="Seleccione las opiniones que quiere visualizar en la serie de tiempo \"Frecuencias de Sentimientos\"",
                                            id="opiniones",
                                            options=[
                                            {"label":"positivos", "value":"Positivos"},
                                            {"label":"negativos", "value":"Negativos"}],
                                            multi=True,
                                            value=["Positivos"]
                                        ),
                                        html.H1(),
                                    ]
                                )
                            ],
                        ),
                        html.Div(
                            id="div-graphs",
                            children=[
                                html.Div(
                                    children = [
                                        #dcc.Graph(figure=fig)
                                        dcc.Graph(className='bg-grey' ,id='indice', figure=fig, config={'displayModeBar': False}, animate=True),
                                        dcc.Graph(className='bg-grey' ,id='timeseries', config={'displayModeBar': False}, animate=True),



                                        #------NUBE DE PALABRAS
                                        html.Div(className='bg-grey',
                                            children = [
                                                html.Div(className = "row",
                                                    children = [
                                                        html.Div(html.H2("Nube de Palabras")),
                                                        html.Div(className= "column",
                                                            children = [
                                                                html.H3("Palabras Positivas"),
                                                                html.Img(src='assets/positivos.jpeg', style={"max-width":"100%", "max-height":"100%","float":"left"})
                                                            ],
                                                            style ={"width":"45%","float":"left"}),
                                                        html.Div(className= "column",
                                                            children = [
                                                                html.H3("Palabras Negativas"),
                                                                html.Img(src='assets/negativos.jpeg', style={"max-width":"100%", "max-height":"100%","float":"left"})
                                                            ],
                                                            style ={"width":"45%","float":"left"})
                                                    ]
                                                )
                                            ]
                                        ),



                                        # ----Gráficas de frecuencias
                                        html.Div(className='bg-grey',
                                            children = [
                                                html.Div(className = "row",
                                                    children = [
                                                        html.Div(html.H2("Gráficas de frecuencias")),
                                                        html.Div(className= "column",
                                                            children = [
                                                                html.H3("Palabras Positivas"),
                                                                html.Img(src='assets/frec_pos.jpeg', style={"max-width":"100%", "max-height":"100%","float":"left"})
                                                            ],
                                                            style ={"width":"45%","float":"left"}),
                                                        html.Div(className= "column",
                                                            children = [
                                                                html.H3("Palabras Negativas"),
                                                                html.Img(src='assets/frec_neg.jpeg', style={"max-width":"100%", "max-height":"100%","float":"left"})
                                                            ],
                                                            style ={"width":"45%","float":"left"})
                                                    ]
                                                )
                                            ]
                                        ),



                                        # ----Accuracy + Dispersión
                                        html.Div(className='bg-grey',
                                            children = [
                                                html.Div(className = "row",
                                                    children = [
                                                        html.Div(),
                                                        html.Div(className= "column",
                                                            children = [
                                                                html.H3("Accuracy de los modelos"),
                                                                html.Img(src='assets/accuracy.png', style={"max-width":"100%", "max-height":"100%","float":"left"})
                                                            ],
                                                            style ={"width":"45%","float":"left"}),
                                                        html.Div(className= "column",
                                                            children = [
                                                                html.H3("Gráficas de dispersión"),
                                                                html.Img(src='assets/disper.jpeg', style={"max-width":"100%", "max-height":"100%","float":"left"})
                                                            ],
                                                            style ={"width":"45%","float":"left"})
                                                    ]
                                                )
                                            ]
                                        )

                                    ]
                                )
                            ]
                        ),
                    ],
                )
            ],
        )

# Actualización
@app.callback(Output('timeseries', 'figure'),
              [Input('opiniones', 'value')])
def update_graph(selected_dropdown_value):
    trace1 = []
    df_sub = df
    print(selected_dropdown_value)
    for stock in selected_dropdown_value:

        trace1.append(go.Scatter(x=df_sub['fecha'],
                                 y=df_sub[stock],
                                 mode='lines',
                                 opacity=0.7,
                                 name=stock,
                                 textposition='bottom center'))
    traces = [trace1]
    data = [val for sublist in traces for val in sublist]
    figure = {'data': data,
              'layout': go.Layout(
                  colorway=["#5E0DAC", '#FF4F00', '#375CB1', '#FF7400', '#FFF400', '#FF0056'],
                  template='plotly_dark',
                  paper_bgcolor='rgba(0, 0, 0, 0)',
                  plot_bgcolor='rgba(0, 0, 0, 0)',
                  margin={'b': 15},
                  hovermode='x',
                  autosize=True,
                  title={'text': 'Frecuencias de Sentimientos', 'font': {'color': 'white'}, 'x': 0.5},
                  xaxis={'range': [df_sub['fecha'].min(), df_sub['fecha'].max()]},
              ),

              }

    return figure


if __name__ == '__main__':
    app.run_server(debug=True)
