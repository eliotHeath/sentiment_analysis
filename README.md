# README #

### Horen ###

![Alt text](BBVA_Hack.png?raw=true "Hackaton 2020")

### Hackers ###

*	Sarahí Lara López : estudiante de Ingeniería en Sistemas Computacionales
*	Felipe Villegas: historiador y pasante de economía
*	Mitzi Camba: Actuaria de la UNAM
*	Miguel Ángel Monroy: matemático
*	José Arturo Barrera: estudiante de la maestría 
*	Eliot.H: Actuario de la UNAM @https://bitbucket.org/eliotHeath/

### Proyecto ###

* La programación presentada en este concurso tiene como resultado un índice de sentimientos al nivel de México. Los sentimientos que se consideraron son los positivos y los negativos. El índice es el resultado del cociente entre ellos dos en cada periodo de tiempo. Para la realización del índice se hizo la comparación de 30 modelos realizados con unigramas, bigramas y trigramas, con procesos de tipo logit, Naive Bayes, Ada Boost, etc. Cada proceso arrojó tres modelos. Se eligió el que tenía el mejor accuracy. La información que se usó como insumo fueron los diccionarios de México proporcionados por TASS. Además, se creó uno propio con una muestra de 150 comentarios aleatorios de los 20,000 mil comentarios de la red social Facebook descargados. De esta manera, se refinó la clasificación usando modismos mexicanos aplicados a una institución financiera.
* Horen 1.0
* [Learn Markdown](https://bitbucket.org/eliotHeath/sentiment_analysis/src/master/)

### Descripción de la solución ###
* La programación presentada en este concurso tiene como resultado un índice de sentimientos al nivel de México. Los sentimientos que se consideraron son los positivos y los negativos. El índice es el resultado del cociente entre ellos dos en cada periodo de tiempo. Para la realización del índice se hizo la comparación de 30 modelos realizados con unigramas, bigramas y trigramas, con procesos de tipo logit, Naive Bayes, Ada Boost, etc. Cada proceso arrojó tres modelos. Se eligió el que tenía el mejor accuracy. La información que se usó como insumo fueron los diccionarios de México proporcionados por TASS. Además, se creó uno propio con una muestra de 150 comentarios aleatorios de los 20,000 mil comentarios de la red social Facebook descargados. De esta manera, se refinó la clasificación usando modismos mexicanos aplicados a una institución financiera.

### Arquitectura ###

![Alt text](Arquitectura.png?raw=true "Hackaton 2020")

### Ejeucio del programa ###

* 	Ejecutar la siguiente instruccion:
* 	python 01_Analisis_de_sentimiento.py

*	Se ejecutaran la limpeza de los datos asi como las nubes de palabras, adems, empezaran a correr los modelos que entrenamos Horen





